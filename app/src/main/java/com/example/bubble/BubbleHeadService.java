package com.example.bubble;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

import java.util.Calendar;

public class BubbleHeadService extends Service {

    private WindowManager mWindowManager;
    private View mChatHeadView;
    private View mChatCloseView;
    private View screenprincipal;

    ImageView closeView;

    public BubbleHeadService(){

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate() {
        super.onCreate();

        //RelativeLayout screen = (RelativeLayout) screenprincipal.findViewById(R.id.Screen);

        int width = getResources().getDisplayMetrics().widthPixels;
        int heigth = getResources().getDisplayMetrics().heightPixels;

        //Le asigno el layout de la burbuja a la variable
        mChatHeadView = LayoutInflater.from(this).inflate(R.layout.bubble, null);
        mChatCloseView = LayoutInflater.from(this).inflate(R.layout.close, null);
        closeView = (ImageView) mChatCloseView.findViewById(R.id.closebtn);

        int LAYOUT_FLAG;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }

        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                LAYOUT_FLAG,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT
        );

        final WindowManager.LayoutParams params2 = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                LAYOUT_FLAG,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT
        );

        final int closePosition = (width/2)-100;

        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.x = 0;
        params.y = 100;

        params2.gravity = Gravity.TOP | Gravity.LEFT;
        params2.x = closePosition;
        params2.y = 1800;


        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        mWindowManager.addView(mChatHeadView, params);
        mWindowManager.addView(mChatCloseView, params2);

        closeView.setVisibility(View.INVISIBLE);

        final ImageView chatHeadImage = (ImageView) mChatHeadView.findViewById(R.id.chat_head_profile_iv);

        chatHeadImage.setOnTouchListener(new View.OnTouchListener() {


            private int lastAction;
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;
            private static final int MAX_CLICK_DURATION = 200;
            private long startClickTime;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        startClickTime = Calendar.getInstance().getTimeInMillis();
                        initialX = params.x;
                        initialY = params.y;

                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();

                        lastAction = event.getAction();

                        return true;
                    case MotionEvent.ACTION_UP:

                        long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                        if (clickDuration < MAX_CLICK_DURATION) {
                            Intent intent = new Intent(BubbleHeadService.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                            startActivity(intent);

                            stopSelf();
                            mWindowManager.removeView(mChatCloseView);
                        }
                        lastAction = event.getAction();

                        closeView.setVisibility(View.INVISIBLE);

                        return true;
                    case MotionEvent.ACTION_MOVE:

                        closeView.setVisibility(View.VISIBLE);

                        params.x = initialX + (int) (event.getRawX() - initialTouchX);
                        params.y = initialY + (int) (event.getRawY() - initialTouchY);

                        if (params.x <=closePosition && params.x >= closePosition-200 && params.y <=1800 && params.y >= 1600){
                            stopSelf();
                            mWindowManager.removeView(mChatCloseView);
                        }

                        //hideButton();

                        mWindowManager.updateViewLayout(mChatHeadView, params);

                        //Log.e("Parametros", "x "+String.valueOf(params.x) +" y "+ String.valueOf(params.y));

                        lastAction = event.getAction();
                        return true;
                }
                return false;
            }

        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mChatHeadView != null) mWindowManager.removeView(mChatHeadView);
    }

}
